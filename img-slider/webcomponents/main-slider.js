class MainSlider extends HTMLElement {

constructor(){
  super();
  this.generateShadowDom();
  var slideIndex = 1;
  this.generateTemplate();
  showSlides(slideIndex);
 
}

  static plusSlides(n) {
  showSlides(slideIndex += n);
}

static currentSlide(n) {
  showSlides(slideIndex = n);
}

static showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) { this.slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[this.slideIndex - 1].style.display = "block";
  dots[this.slideIndex - 1].className += " active";
}

  static get is(){
    return 'main-slider';
  }

  generateShadowDom(){
    this.attachShadow({mode:'open'});
  }

  generateTemplate(){
    const template = document.createElement('template');
    template.innerHTML = 
  `<div id="main-content">
    <div class="slideshow-container">

        <div class="mySlides fade">
            <div class="numbertext">1 / 3</div>
            <img src="/images/img1.jpg" style="width:100%">
            <div class="text">Caption Text</div>
        </div>

        <div class="mySlides fade">
            <div class="numbertext">2 / 3</div>
            <img src="/images/img2.jpg" style="width:100%">
            <div class="text">Caption Two</div>
        </div>

        <div class="mySlides fade">
            <div class="numbertext">3 / 3</div>
            <img src="/images/img3.jpg" style="width:100%">
            <div class="text">Caption Three</div>
        </div>

        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>

    <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>
</div>`;
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}
window.customElements.define(MainSlider.is, MainSlider);